﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CodeFirstDB
{
  public  class Bilgiler
    {

        public int BilgilerID { get; set; }

        public bool FormTuru { get; set; }
        public  Sirket Sirket { get; set; }

        public DateTime Donem { get; set; }

        public int Adet { get; set; }

        public double Tutar { get; set; }


    

        public int MuhasebeciID { get; set; }
        public Muhasabeci Muhasabeci { get; set; }


    }
}
