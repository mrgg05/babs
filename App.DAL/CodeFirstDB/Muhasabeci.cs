﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CodeFirstDB
{
   public class Muhasabeci
    {
        [Key]
        public int MuhasabeciID { get; set; }
        public string MhbAdi { get; set; }
        public string MhbSoyadi { get; set; }

        public string MhbTel { get; set; }

        public string MhbMail { get; set; }

        public string Password { get; set; }

        [ForeignKey("MusavirID")]
        public int MusavirID { get; set; }
        public Musavir Musavir { get; set; }

      //  [ForeignKey("BilgilerID")]
        public List<Bilgiler> Bilgiler { get; set; }


    }
}
