﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CodeFirstDB
{
   public class BabsContext:DbContext
    {

        public DbSet<Musavir> Musavir { get; set; }

        public DbSet<Muhasabeci> Muhasebeci { get; set; }
        public DbSet<Sirket> Sirket { get; set; }

        public DbSet<Bilgiler> Bilgiler { get; set; }


    }
}
