﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CodeFirstDB
{
   public class Musavir
    {
        [Key]
        public int MusavirID { get; set; }
        public string MusavirAdi { get; set; }

        public string MusavirSoyadi { get; set; }

        public string MusavirTel { get; set; }

        public string MusavirMail { get; set; }
        public string Password { get; set; }



        //[ForeignKey("MhbID")]

        public List<Muhasabeci> Muhasabeci { get; set; }


      //  [ForeignKey("SirketID")]
        public List<Sirket> Sirket { get; set; }




    }
}
