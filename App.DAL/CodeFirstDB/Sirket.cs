﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CodeFirstDB
{
   public class Sirket
    {
        [Key]
        public int SirketID { get; set; }

        public string SirketUnvani { get; set; }

      
        public string SirketVergiNo { get; set; }

        public string SirketTel { get; set; }
        public string SirketMail { get; set; }


        [ForeignKey("MusavirID")]

        public int MusavirID { get; set; }
        public Musavir Musavir { get; set; }





    }
}
