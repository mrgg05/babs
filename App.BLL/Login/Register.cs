﻿using App.BLL.Repository;
using App.DAL.CodeFirstDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Login
{
  public  class Register
    {

        MuhasebeciRepository mr = new MuhasebeciRepository();
        MusavirRepository musr = new MusavirRepository();
        BabsContext dbb = new BabsContext();

        public string Adi { get; set; }
        public string Soyadi { get; set; }

        public string Mail { get; set; }
        public string Tel { get; set; }

       
        public void MuhasebeciKayit(string ad,string soyad, string mail, string tel, string pass)
        {

            mr.Insert(new Muhasabeci() { MhbAdi = ad,MhbSoyadi=soyad,MhbMail=mail,MhbTel=tel,Password=pass});
            //id = dbb.Muhasebeci.OrderByDescending(x => x.MhbID).FirstOrDefault().MhbID;
        }
        
        public void MusavirKayit(string ad, string soyad, string mail, string tel, string pass)
        {

            musr.Insert(new Musavir() { MusavirAdi = ad, MusavirSoyadi = soyad, MusavirMail = mail, MusavirTel = tel, Password = pass });

        }


    }
}
