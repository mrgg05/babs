﻿using App.BLL.Repository;
using App.DAL.CodeFirstDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL
{
   public class BilgilerKayit
    {

        BilgilerRepository br = new BilgilerRepository();
        BabsContext db = new BabsContext();

        public void BilgilerKyt(string donem, int adet, double tutar, bool tur)
        {

            br.Insert(new Bilgiler() { Donem =DateTime.Parse(donem), Adet = adet, Tutar = tutar, FormTuru = tur, });

        }

    }
}
