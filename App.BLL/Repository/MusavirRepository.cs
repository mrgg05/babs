﻿using App.DAL.CodeFirstDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Repository
{
    public class MusavirRepository : IRepository<Musavir>
    {
        BabsContext db = new BabsContext();
        public void Delete(int item)
        {
            db.Muhasebeci.Remove(db.Muhasebeci.Find(item));
            db.SaveChanges();
        }

        public List<Musavir> GetAll()
        {
            return db.Musavir.ToList();
        }

        public void Insert(Musavir item)
        {
            db.Musavir.Add(item);
            db.SaveChanges();
        }
        public Musavir SelectById(int itemId)
        {
            return db.Musavir.Find(itemId);
        }

        public void Update(Musavir item)
        {
            Musavir updated = db.Musavir.Find(item.MusavirID);
            db.Entry(updated).CurrentValues.SetValues(item);
            db.SaveChanges();

          
        }
    }
}
