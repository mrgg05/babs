﻿using App.DAL.CodeFirstDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Repository
{
    public class SirketRepository : IRepository<Sirket>
    {

        BabsContext db = new BabsContext();

        public void Delete(int item)
        {
            db.Sirket.Remove(db.Sirket.Find(item));
            db.SaveChanges();
        }

        public List<Sirket> GetAll()
        {
            return db.Sirket.ToList();
        }

        public void Insert(Sirket item)
        {
            db.Sirket.Add(item);
            db.SaveChanges();
        }
        public Sirket SelectById(int itemId)
        {
            return db.Sirket.Find(itemId);
        }
        public void Update(Sirket item)
        {
            Sirket updated = db.Sirket.Find(item.SirketID);
            db.Entry(updated).CurrentValues.SetValues(item);
            db.SaveChanges();
        }
    }
}
