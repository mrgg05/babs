﻿using App.DAL.CodeFirstDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Repository
{
  public  class BilgilerRepository : IRepository<Bilgiler>
    {


        BabsContext db = new BabsContext();
        public void Delete(int item)
        {
            db.Bilgiler.Remove(db.Bilgiler.Find(item));
            db.SaveChanges();
        }

        public List<Bilgiler> GetAll()
        {
            return db.Bilgiler.ToList();
        }

        public void Insert(Bilgiler item)
        {
            db.Bilgiler.Add(item);
            db.SaveChanges();
        }
        public Bilgiler SelectById(int itemId)
        {
            return db.Bilgiler.Find(itemId);
        }

        public void Update(Bilgiler item)
        {
            Bilgiler updated = db.Bilgiler.Find(item.BilgilerID);
            db.Entry(updated).CurrentValues.SetValues(item);
            db.SaveChanges();
        }
    }
}
