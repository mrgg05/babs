﻿using App.DAL.CodeFirstDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Repository
{
    public class MuhasebeciRepository : IRepository<Muhasabeci>
    {
        BabsContext db = new BabsContext();
        public void Delete(int item)
        {

            db.Muhasebeci.Remove(db.Muhasebeci.Find(item));
            db.SaveChanges();

        }

        public List<Muhasabeci> GetAll()
        {
          return  db.Muhasebeci.ToList();
        }

        public void Insert(Muhasabeci item)
        {
            db.Muhasebeci.Add(item);
            db.SaveChanges();
        }
        public Muhasabeci SelectById(int itemId)
        {
            return db.Muhasebeci.Find(itemId);
        }

        public void Update(Muhasabeci item)
        {

            //Muhasabeci eskiHali = db.Muhasebeci.Where(x => x.MhbID == item.MhbID).FirstOrDefault();

            //eskiHali.Musavir = item.Musavir;
            //int guncellenen = db.SaveChanges();




            Muhasabeci updated = db.Muhasebeci.Find(item.MuhasabeciID);
            db.Entry(updated).CurrentValues.SetValues(item);
            db.SaveChanges();
        }
    }
}
