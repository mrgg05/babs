﻿using App.BLL.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App.WinUI
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            comboBox1.Text = "Seçiniz...";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Register register = new Register();

            if (comboBox1.Text=="Seçiniz...")
            {
                MessageBox.Show("Lütfen Meslek Seçiniz.");
            }
            else
            {
                if (comboBox1.Text == "Muhasebeci")
                {
                    register.MuhasebeciKayit(textBox1.Text, textBox2.Text, textBox3.Text, maskedTextBox1.Text, textBox4.Text);
                }
                if (comboBox1.Text == "Musavir")
                {
                    register.MusavirKayit(textBox1.Text, textBox2.Text, textBox3.Text, maskedTextBox1.Text, textBox4.Text);
                }
                MessageBox.Show("Başarılı Kayıt.");

                Form1 frm = new Form1();
                frm.Show();
                this.Hide();
            }
           

        }
    }
}
