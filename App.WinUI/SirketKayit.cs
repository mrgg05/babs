﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.BLL.Login;

namespace App.WinUI
{
    public partial class SirketKayit : Form
    {
        public SirketKayit()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SirketRegister sr = new SirketRegister();

            if (string.IsNullOrWhiteSpace(textBox1.Text) && string.IsNullOrWhiteSpace(textBox2.Text) && string.IsNullOrWhiteSpace(maskedTextBox1.Text) && string.IsNullOrWhiteSpace(maskedTextBox2.Text))
            {
                MessageBox.Show("Lütfen Bilgilerinizi Boş Bırakmayınız.");
            }
            else
            {
                sr.SirketKayit(textBox1.Text, maskedTextBox2.Text, textBox2.Text, maskedTextBox1.Text);
                MessageBox.Show("Başarılı kayıt");
               
            }
            

        }
    }
}
